
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    public static void main(String[] args) {
        List<Jogador> osjogador = new ArrayList<>();
        Scanner scnj = new Scanner(System.in);
        System.out.println("Digite o numero de jogadores do time 1: ");
            try{
                int nj1 = scnj.nextInt();
                for (int i=0; i<nj1; i++){
                    Scanner scn = new Scanner(System.in);
                    System.out.println("Digite o numero do jogador "+ (i+1) + ": ");
                    int nt1=-1;
                    if(scn.hasNextInt())
                        nt1 = scn.nextInt();
                    if (nt1>0){
                        Scanner scj = new Scanner(System.in);
                        String j1 = "";
                        if (scj.hasNextLine())
                            j1 = scj.nextLine();
                        osjogador.add(new Jogador(nt1, j1));
                    }

                }
                JogadorComparator joga = new JogadorComparator(true, true, false);
                osjogador.sort(joga);
                for(Jogador a:osjogador)
                    System.out.println(a);
                int a = 1;
                while(a!=0){
                    boolean ja = false;
                    Scanner sca = new Scanner(System.in);
                    Scanner scb = new Scanner(System.in);
                    if(sca.hasNextInt())
                        a= sca.nextInt();
                    String b = "";
                    if (a!=0)
                        if (scb.hasNextLine())
                            scb.nextLine();
                    for(Jogador anb:osjogador){
                        if (anb.numero==a){
                            anb.nome = b;
                            ja = true;
                        }
                    }
                    if (!ja && a!=0){
                        osjogador.add(new Jogador(a, b));
                        osjogador.sort(joga);
                    }
                }
                for(Jogador adffd:osjogador)
                    System.out.println(adffd);
            }catch(InputMismatchException aa){
                System.out.println(aa.getLocalizedMessage());
            }
        }
}
